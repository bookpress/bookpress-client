<?php
/*
Plugin Name: BookPress Client
Author: BookPress Team
Plugin URI: http://bookpress.me/
Description: This plugin enables you to publish ebooks from your existing WordPress contents. Choose posts to be published, edit them( which won't reflect to the original ) and click "Publish" button.
Version: 0.4.0
Author URI: http://bookpress.me/
Domain Path: /languages
Text Domain: bookpress
*/

register_activation_hook(__FILE__, 'bookpress_activation');
register_deactivation_hook(__FILE__, 'bookpress_deactivation');

add_action('wpmu_new_blog', 'bookpress_activation');

function bookpress_activation() {
    if (!get_option(BookPress::api_key)) {
        update_option(BookPress::api_key, BookPress::get_uniq_key());
    }
    if (!get_option(BookPress::option_books)) {
        update_option(BookPress::option_books, array(
            BookPress::get_book_id() => array(
                'name' => 'My Book (1)',
                'title' => get_bloginfo('name'),
            ),
        ));
    }
    BookPress::init();
    flush_rewrite_rules();
}

function bookpress_deactivation() {
    flush_rewrite_rules();
}

new BookPress();

class BookPress {

const version = '0.2.0';
const query_var = 'bookpress';
const api_key = 'bookpress-api';
const nonce_key = 'bookpress-nonce';
const option_books = 'bookpress-mybooks';

function __construct()
{
    add_action('plugins_loaded', array(&$this, 'plugins_loaded'));
}

public function plugins_loaded()
{
    add_action('init', array(&$this, 'init'));
    add_filter('query_vars', array(&$this, 'query_vars'));
    add_action('template_redirect', array(&$this, 'template_redirect'));
    add_action("admin_bar_menu", array(&$this, "admin_bar_menu"), 9999);
    //add_action('pre_ping', array(&$this, 'pre_ping'));
    if (is_admin()) {
        add_action("admin_menu", array(&$this, "admin_menu"));
        add_action("admin_init", array(&$this, "admin_init"));
        add_action('admin_footer-post-new.php', array(&$this, 'admin_footer'));
        add_action('pre_get_posts', array(&$this, 'pre_get_posts'));
    }
}

/*
public function pre_ping(&$links)
{
    if ($this->is_book(get_post_type())) {
        $links = array();
    }
}
*/

public function pre_get_posts($wp_query)
{
    $books = get_option(BookPress::option_books);
    if (isset($_GET['post_type']) && in_array($_GET['post_type'], array_keys($books))) {
        if (!isset($wp_query->query_vars['orderby'])) {
            $wp_query->query_vars['orderby'] = 'menu_order';
        }
        if (!isset($wp_query->query_vars['order'])) {
            $wp_query->query_vars['order'] = 'ASC';
        }
    }
}

public function admin_bar_menu($bar)
{
    if (current_user_can("edit_posts")) {
        if (is_singular()) {
            $bar->add_menu(array(
                "id"    => "bookpress",
                "title" => "BookPress",
                "href"  => false,
            ));
            $books = get_option(BookPress::option_books);
            foreach ($books as $id => $book) {
                $url = sprintf(
                    'post-new.php?post_type=%s&origin=%d',
                    $id,
                    get_the_ID()
                );
                $bar->add_menu(array(
                    "parent" => "bookpress",
                    "id"    => $id,
                    "title" => sprintf(
                        __("Copy to &quot;%s&quot;", "bookpress"),
                        $book['name']
                    ),
                    "href"  => admin_url($url),
                    "meta"  => false,
                ));
            }
        }
    }
}

public function admin_footer()
{
    $books = get_option(BookPress::option_books);
    if (isset($books[get_post_type()])) {
        if (isset($_GET['origin']) && intval($_GET['origin'])) {
            $origin = get_post(intval($_GET['origin']));
            if ($origin) {
                $template = array(
                    'post_title' => $origin->post_title,
                    'post_content' => apply_filters('the_content', ($origin->post_content)),
                );
                $template = json_encode($template);
                echo <<<EOL
<script type="text/javascript">
var origin = {$template};
jQuery('#title').val(origin.post_title);
jQuery('#content').val(origin.post_content);
</script>
EOL;
            }
        }
    }
}

public function admin_init()
{
    if (!current_user_can('update_core')) {
        return;
    }

    if (isset($_GET['page']) && $_GET['page'] === 'bookpress-settings') {
        if (isset($_GET['regenerate']) && wp_verify_nonce($_GET['regenerate'], self::nonce_key)) {
            update_option(BookPress::api_key, $this->get_uniq_key());
            $url = admin_url('admin.php?page=bookpress-settings');
            set_transient('bookpress-updated', 1, 5);
            wp_redirect($url);
            exit;
        }
        if (isset($_POST['delete-book']) && wp_verify_nonce($_POST['delete-book'], self::nonce_key)) {
            $posts = get_posts(array(
                'post_type' => $_POST['id'],
                'post_status' => 'any',
            ));
            foreach ($posts as $p) {
                wp_delete_post($p->ID, true);
            }
            $books = get_option(self::option_books);
            unset($books[$_POST['id']]);
            update_option(self::option_books, $books);
            $url = admin_url('admin.php?page=bookpress-settings');
            set_transient('bookpress-updated', 1, 5);
            wp_redirect($url);
            exit;
        }
    }

    if (isset($_GET['page']) && $_GET['page'] === 'bookpress-manage-books') {
        if (isset($_POST['update-book']) && wp_verify_nonce($_POST['update-book'], self::nonce_key)) {
            $args = array(
                'input-label',
                'input-title',
                'input-cover',
            );
            foreach ($args as $arg) {
                if (isset($_POST[$arg]) && strlen($_POST[$arg])) {
                    $_POST[$arg] = trim($_POST[$arg]);
                } else {
                    wp_die('All fields are required.');
                }
            }
            $books = get_option(self::option_books);
            $books[$_POST['id']] = array(
                'name'  => $_POST['input-label'],
                'title' => $_POST['input-title'],
                'cover' => $_POST['input-cover'],
            );
            update_option(self::option_books, $books);
            $url = admin_url('admin.php?page=bookpress-manage-books#'.$_POST['id']);
            set_transient('bookpress-updated', 1, 5);
            wp_redirect($url);
            exit;
        }
        if (isset($_POST['add-book']) && wp_verify_nonce($_POST['add-book'], self::nonce_key)) {
            $args = array('new-book');
            foreach ($args as $arg) {
                if (isset($_POST[$arg]) && strlen($_POST[$arg])) {
                    $_POST[$arg] = trim($_POST[$arg]);
                } else {
                    wp_die('All fields are required.');
                }
            }
            $books = get_option(self::option_books);
            $books[self::get_book_id()] = array(
                'name'  => $_POST['new-book'],
                'title' => get_bloginfo('name'),
                'cover' => '',
            );
            update_option(self::option_books, $books);
            $url = admin_url('admin.php?page=bookpress-manage-books');
            set_transient('bookpress-updated', 1, 5);
            wp_redirect($url);
            exit;
        }
    }
}

public function admin_menu()
{
    global $menu;
    $max = intval(max(array_keys($menu))) + 1;
    $menu[$max] = array(
        '',
        'read',
        "separator-bookpress",
        '',
        'wp-menu-separator'
    );
    $max = $max + 1;

    $hook = add_menu_page(
        "BookPress",
        "BookPress",
        "administrator",
        "bookpress-manage-books",
        false,
        plugins_url('img/book-icon.png', __FILE__),
        $max
    );

    $hook = add_submenu_page(
        "bookpress-manage-books",
        "Manage Books",
        "Manage Books",
        "administrator",
        "bookpress-manage-books",
        array(&$this, "admin_panel")
    );
    add_action('admin_print_styles-'.$hook, array(&$this, 'admin_styles'));
    add_action('admin_print_scripts-'.$hook, array(&$this, 'admin_scripts'));

    $hook = add_submenu_page(
        "bookpress-manage-books",
        "Settings",
        "Settings",
        "administrator",
        "bookpress-settings",
        array(&$this, "admin_panel")
    );
    add_action('admin_print_styles-'.$hook, array(&$this, 'admin_styles'));
    add_action('admin_print_scripts-'.$hook, array(&$this, 'admin_scripts'));

    $max = $max + 1;
    $temp_menu = $menu; // これやらないと無限ループ
    foreach ($temp_menu as $key => $item) {
        if (isset($item[6]) && preg_match('/bookpress-client\/img\/book-icon2.png/', $item[6])) {
            $menu[$max++] = $item;
            unset($menu[$key]);
        }
    }
    ksort($menu); // これをやらないとセパレーターが出ない
}


public function admin_scripts()
{
    wp_enqueue_script(
        'bookpress-bootstrap-tab',
        plugins_url("js/bootstrap-tab.js", __FILE__),
        array('jquery'),
        filemtime(dirname(__FILE__).'/js/bootstrap-tab.js'),
        true
    );
    wp_enqueue_script(
        'bookpress-bootstrap-modal',
        plugins_url("js/bootstrap-modal.js", __FILE__),
        array('jquery'),
        filemtime(dirname(__FILE__).'/js/bootstrap-modal.js'),
        true
    );
    wp_enqueue_script(
        'bookpress-admin-script',
        plugins_url("js/bookpress.js", __FILE__),
        array(
            'bookpress-bootstrap-tab',
            'bookpress-bootstrap-modal',
        ),
        filemtime(dirname(__FILE__).'/js/bookpress.js'),
        true
    );

    if (isset($_GET['page']) && $_GET['page'] === 'bookpress-manage-books') {
        wp_enqueue_script(
            'my-media-uploader',
            plugins_url("js/media.js", __FILE__),
            array('jquery'),
            filemtime(dirname(__FILE__).'/js/media.js'),
            false
        );
        wp_enqueue_media();
    }
}

public function admin_styles()
{
    wp_enqueue_style(
        'bookpress-admin-style',
        plugins_url("css/style.css", __FILE__),
        array(),
        filemtime(dirname(__FILE__).'/css/style.css')
    );
}

public function admin_panel()
{
?>
<div id="bookpress-admin" class="wrap">
<div class="row-fluid">
<h2>BookPress</h2>
<?php if (get_transient('bookpress-updated')) : ?>
<div class="alert alert-success">
Updated!
</div>
<?php endif; ?>
<?php
    if (isset($_GET['page']) && $_GET['page'] === 'bookpress-manage-books') {
        require_once(dirname(__FILE__).'/admin/manage-books.php');
    } elseif (isset($_GET['page']) && $_GET['page'] === 'bookpress-settings') {
        require_once(dirname(__FILE__).'/admin/settings.php');
    }
?>
</div><!-- .row-fluid -->
</div><!-- #bookpress-admin -->
<?php
}

public function query_vars($vars)
{
    $vars[] = self::query_var;
    return $vars;
}

public function init()
{
    add_rewrite_endpoint(self::query_var, EP_ROOT);
    $books = get_option(self::option_books);

    foreach ($books as $id => $ops) {
        self::create_books($id, $ops);
    }
}

private function create_books($id, $ops)
{
    $args = array(
        'label' => __($ops['name'], 'bookpress'),
        'labels' => array(
            'singular_name' => __('Pages', 'bookpress'),
            'add_new_item' => __('Add New Page', 'bookpress'),
            'edit_item' => __('Edit Page', 'bookpress'),
            'add_new' => __('Add New', 'bookpress'),
            'new_item' => __('New Page', 'bookpress'),
            'view_item' => __('View Page', 'bookpress'),
            'not_found' => __('No pages found.', 'bookpress'),
            'not_found_in_trash' => __(
                'No pages found in Trash.',
                'bookpress'
            ),
            'search_items' => __('Search Pages', 'bookpress'),
        ),
        'public' => false,
        'publicly_queryable' => false,
        'exclude_from_search' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => false,
        'show_in_nav_menus' => false,
        'can_export' => false,
        'menu_icon' => plugins_url('img/book-icon2.png', __FILE__),
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'revisions',
            'page-attributes',
            'thumbnail',
        )
    );
    register_post_type($id, $args);
}

public function plugins_url()
{
    return plugins_url('', __FILE__);
}

public function template_redirect()
{
    global $wp_query;
    if (isset($wp_query->query[self::query_var])) {
        if (isset($_GET['key']) && ($_GET['key'] === get_option(self::api_key))) {
            require_once(dirname(__FILE__).'/includes/exporter.class.php');
            $exp = new BookPressExporter();
            $exp->output();
            exit;
        }
        $this->send404();
    }
}

public function send404()
{
    global $wp_query;
    $wp_query->set_404();
    status_header(404);
}

public function get_uniq_key()
{
    return md5(uniqid(rand(), true));
}

public function get_book_id()
{
    $res = wp_remote_get('http://bookpress.me/api/get_id/');
    if ($res['response']['code'] === 200) {
        return json_decode($res['body']);
    } else {
        return substr(md5(uniqid(rand(), true)), 0, 16);
    }
}

private function is_book($post_type)
{
    $books = get_option(BookPress::option_books);
    $book_ids = array_keys($books);
    if (in_array($post_type, $book_ids)) {
        return true;
    } else {
        return false;
    }
}

} // end class

// EOF
