(function($){
    $('#site-key').click(function(){
        $('#site-key').select();
    });

    $('.books:first').addClass('active');
    if ($(".books").length > 1) {
        $('.books').each(function(i){
            var id = $(this).attr('id');
            var label_text = $('.book-label:first', this).text();
            var label = $('<span />').text(label_text).html();
            if (i === 0) {
                $('#books-tab').append(
                    '<li class="active"><a href="#'+id+'" data-toggle="tab"><i class="icon-book"></i> '+label+'</a></li>'
                );
            } else {
                $('#books-tab').append(
                    '<li><a href="#'+id+'" data-toggle="tab"><i class="icon-book"></i> '+label+'</a></li>'
                );
            }
        });
        if (location.hash) {
            $('#books-tab a').each(function(){
                if ($(this).attr('href') === location.hash) {
                    $(this).tab('show');
                }
            });
        }
    }

    if ($('#settings').length) {
        $('input[name="delete-book"]').change(function(){
            if ($(this).is(':checked')) {
                $('#delete-id').val($(this).val());
                $('#delete-form button[type="submit"]').attr('disabled', false);
            } else {
                $('#delete-id').val('');
                $('#delete-form button[type="submit"]').attr('disabled', true);
            }
        });
        $('#delete-form').submit(function(){
            if (confirm('Are you sure you want to delete ?')) {
                return true;
            } else {
                return false;
            }
        });
    }
})(jQuery);
