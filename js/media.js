jQuery(document).ready(function($){
    var bookpress_uploader;
    var book_id;
    $('.upload_image_button').click(function(e) {
        book_id = $(this).attr('data-id');
        e.preventDefault();
        if (bookpress_uploader) {
            bookpress_uploader.open();
            return;
        }
        bookpress_uploader = wp.media({
            title: 'Choose Cover Image',
            library: {
                type: 'image'
            },
            button: {
                text: 'Choose Cover Image'
            },
            multiple: false
        });
        bookpress_uploader.on('select', function() {
            attachment = bookpress_uploader.state().get('selection').first().toJSON();
            $('#input-cover-'+book_id).val(attachment.url);
            $('#cover-preview-'+book_id).html('<img src="'+attachment.url+'" />');
        });
        bookpress_uploader.open();
    });
});
