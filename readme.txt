=== BookPress Client ===
Contributors: miyauchi,bookpress
Tags: kindle, epub, ebook, e-book
Requires at least: 3.5
Tested up to: 3.5.1
Stable tag: 0.4.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This plugin enables you to publish your WordPress contents as eBooks. Choose posts to be published, edit them and click "Publish" button. Files created by BookPress is Compatible to Kindle, ePub (Kobo is also comming). Easy :)

== Description ==

BookPress Client is a plugin which enables you to publish your posts as an eBook.

We support ePub and Kindle mobi formats. Kobo comming soon.

Please sign up through [our site http://bookpress.me/](http://bookpress.me/).

= Features =

* It's free!
* For a connection authorization between your WordPress and our contents-converting server, registration through [our site](http://bookpress.me/) is required.
* You can create eBooks from your posts that's already written. We copy the posts as a new Custom Post Type posts so that your editing won't affect the posts published as blog posts.
* You can add new posts which is not to be published on your site.
* Set cover image to be shown on kindle store or other stores.
* Multiple books from a single site.


= Support =

info@bookpress.me
[@BookPressMe on twitter](http://twitter.com/bookpressme)
[Our facebook page](https://www.facebook.com/bookpress.me)

== Installation ==

1. Install The BookPress Client Plugin either via the WordPress.org plugin directory, or by uploading the files to your server.
2. Activate the plugin.
3. Open 'BookPress' > 'Setting' menu and copy your site key.
4. Go to the BookPress Web App page and signup.
5. Paste your site key.

== Screenshots ==

1. Book Management page. Name your eBook and set a cover image.
2. We give you an site key. Easy setup!
3. Go to your post page to be published and click "Copy to book-name"
4. List of the posts. You can add new posts too.
5. Activate your account and authorize your WordPress.
6. Choose formats and click "Bind!" button.

== Changelog ==

= 0.1.0 =
* Beta Release.
