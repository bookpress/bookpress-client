<?php

class BookPressExporter {

private $bid = '';

function __construct()
{
    if (isset($_GET['bookid']) && $_GET['bookid']) {
        $this->bid = $_GET['bookid'];
    }
}

public function output()
{
    $books = get_option(BookPress::option_books);
    if ($this->bid) {
        if (!isset($books[$this->bid]) || !$books[$this->bid]) {
            BookPress::send404();
            return;
        }
        // export page
        if (isset($_GET['pid']) && intval($_GET['pid'])) {
            $p = get_post($_GET['pid']);
            if ($p && ($p->post_type === $this->bid)) {
                require_once(dirname(__FILE__).'/filters.class.php');
                add_filter(
                    "use_default_gallery_style",
                    "__return_false"
                );
                $content = apply_filters(
                    'bookpress_post_content',
                    $p->post_content
                );
                $data = array(
                    'ID' => $p->ID,
                    'post_date_gmt' => $p->post_date_gmt,
                    'post_title' => $p->post_title,
                    'post_excerpt' => $p->post_excerpt,
                    'post_modified_gmt' => $p->post_modified_gmt,
                    'post_thumbnail' => wp_get_attachment_url(
                        get_post_thumbnail_id($p->ID)
                    ),
                    'post_content' => $content
                );
                $this->header();
                echo json_encode($data);
                exit;
            } else {
                BookPress::send404();
                return;
            }
        // export pages with title
        } else {
            $book = array();
            $book['title'] = $books[$this->bid]['title'];
            if (isset($books[$this->bid]['cover'])) {
                $book['cover'] = $books[$this->bid]['cover'];
            } else {
                $book['cover'] = '';
            }
            $posts = get_posts(array(
                'numberposts' => -1,
                'post_type' => $this->bid,
                'post_status' => 'publish',
                'orderby' => 'menu_order',
                'order' => 'ASC',
            ));
            $book['pages'] = array();
            foreach ($posts as $p) {
                $book['pages'][] = array(
                    'ID' => $p->ID,
                    'post_date_gmt' => $p->post_date_gmt,
                    'post_title' => $p->post_title,
                    'post_excerpt' => $p->post_excerpt,
                    'post_modified_gmt' => $p->post_modified_gmt,
                    'post_thumbnail' => wp_get_attachment_url(
                        get_post_thumbnail_id($p->ID)
                    ),
                );
            }
            $this->header();
            echo json_encode($book);
            exit;
        }
    } else {
        $this->header();
        $books = get_option(BookPress::option_books);
        echo json_encode($books);
        exit;
    }

    // if not exit send 404 error
}

private function header()
{
    nocache_headers();
    header('Content-type: application/json; charset=UTF-8');
}

} // end class

// EOF
