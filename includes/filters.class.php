<?php

new BookPressFilters();

class BookPressFilters {

private $allowed = array(
    'a' => array(
        'href' => array(),
        'name' => array(),
    ),
    'b' => array(
    ),
    'big' => array(
    ),
    'blockquote' => array(
    ),
    'br' => array(
    ),
    'center' => array(
    ),
    'cite' => array(
    ),
    'dd' => array(
        'title' => array(),
    ),
    'del' => array(
    ),
    'dfn' => array(
    ),
    'div' => array(
        'align' => array(),
        'bgcolor' => array(),
    ),
    'em' => array(
        'title' => array(),
    ),
    'font' => array(
        'color' => array(),
        'face' => array(),
        'size' => array(),
    ),
    'h1' => array(
    ),
    'h2' => array(
    ),
    'h3' => array(
    ),
    'h4' => array(
    ),
    'h5' => array(
    ),
    'h6' => array(
    ),
    'hr' => array(
        'color' => array(),
        'width' => array(),
    ),
    'i' => array(
        'class' => array(),
        'id' => array(),
    ),
    'img' => array(
        'align' => array(),
        'border' => array(),
        'height' => array(),
        'src' => array(),
        'width' => array(),
    ),
    'li' => array(
        'title' => array(),
    ),
    'ol' => array(
    ),
    'p' => array(
        'align' => array(),
        'title' => array(),
    ),
    'rt' => array(
    ),
    'ruby' => array(
    ),
    's' => array(
        'title' => array(),
    ),
    'small' => array(
    ),
    'span' => array(
        'bgcolor' => array(),
        'title' => array(),
    ),
    'strike' => array(
    ),
    'strong' => array(
    ),
    'sub' => array(
    ),
    'sup' => array(
    ),
    'u' => array(
    ),
    'ul' => array(
    ),
    'var' => array(
    ),
    'pre' => array(
    ),
    'code' => array(
    ),
);

function __construct()
{
    add_filter('bookpress_post_content', array(&$this, 'the_content'), 10);
    add_filter('bookpress_post_content', array(&$this, 'wp_kses'), 10);
    add_filter('bookpress_post_content', array(&$this, 'trim'), 9999);
    //add_filter('bookpress_post_content', array(&$this, 'image2scheme'), 9999);
}

public function the_content($content)
{
    return apply_filters('the_content', $content);
}

public function trim($content)
{
    return trim($content);
}

public function wp_kses($content)
{
    $content = wp_kses(
        $content,
        $this->get_allowed_tags(),
        array('http', 'https')
    );
    return $content;
}

/*
public function image2scheme($content)
{
    require_once(dirname(__FILE__).'/parser.class.php');
    $p = new ParseWPContent($content);
    $content = $p->images2scheme();
    return $content;
}
*/

private function get_allowed_tags()
{
    foreach ($this->allowed as $tag => $args) {
        $this->allowed[$tag]['id'] = array();
        $this->allowed[$tag]['class'] = array();
        $this->allowed[$tag]['style'] = array();
    }
    return apply_filters('bookpress_allowed_tags', $this->allowed);
}

} // end class

// EOF
