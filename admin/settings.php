<div id="settings">
<h3>Site Key</h3>

<ol>
<li>Copy the Site Key below.</li>
<li>Go to the <a href="http://bookpress.me/app/?url=<?php echo esc_url(home_url()); ?>">BookPress Web App</a> and register.</li>
<li>Have fun.</li>
</ol>

<div class="input-append">
<input id="site-key" type="text" readonly="readonly" class="span3" value="<?php echo get_option(self::api_key); ?>"  />
<a class="btn" href="http://bookpress.me/app/?url=<?php echo esc_url(home_url()); ?>">Go!</a>
</div>
<a href="<?php echo admin_url("admin.php?page=bookpress-settings&regenerate=".esc_attr(wp_create_nonce(self::nonce_key))); ?>">Re-Generate the Site Key.</a>


<div class="well" style="background-color: #ffffff; margin-top: 3em; overflow: auto;">
<h3 style="margin-top: 0;">Delete Book</h3>
<div class="alert alert-error"><strong>Warning!</strong> There is no going back.</div>
<ul style="margin-top: 2em;">
<?php foreach(get_option(self::option_books) as $id => $ops): ?>
<li><label class="radio"><input type="radio" name="delete-book" value="<?php echo $id; ?>" /> <?php echo $ops['name']; ?></label></li>
<?php endforeach; ?>
</ul>

<form id="delete-form" action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
    <input type="hidden" id="delete-id" name="id" value="" />
    <input type="hidden" name="delete-book" value="<?php echo wp_create_nonce(self::nonce_key); ?>" />
    <p><button type="submit" disabled="disabled" class="btn btn-danger btn-large pull-right">Delete Book</button></p>
</form>
</div>




</div><!-- #settings -->

