<div id="manage-books">

<form action="<?php echo admin_url('admin.php?page=bookpress-manage-books'); ?>" method="post">
<input type="hidden" name="add-book" value="<?php echo wp_create_nonce(self::nonce_key); ?>" />
<div class="input-append">
  <input class="span2" id="new-book" name="new-book" placeholder="My Book" type="text">
  <button class="btn" type="submit"> Add New Book!</button>
</div>
</form>

<h3>Manage Books</h3>

<div class="tabbable tabs-left">
    <ul id="books-tab" class="nav nav-tabs"></ul>
    <div class="tab-content">
<?php foreach(get_option(self::option_books) as $id => $ops): ?>
    <div class="books tab-pane well" id="<?php echo esc_attr($id); ?>" style="overflow: auto;">
    <h4 class="book-label"><?php echo esc_html($ops['name']); ?></h4>
    <form action="<?php echo admin_url('admin.php?page=bookpress-manage-books'); ?>" method="post">
        <input type="hidden" name="id" value="<?php echo esc_attr($id); ?>" />
        <input type="hidden" name="update-book" value="<?php echo wp_create_nonce(self::nonce_key); ?>" />
        <div class="control-group">
            <label class="control-label" for="input-label">Menu Name</label>
            <p><?php _e("eBook Name for display on WordPress admin menu.", 'bookpress'); ?></p>
            <div class="controls">
                <input type="text" data-id="<?php echo esc_attr($id); ?>" name="input-label" class="span2" value="<?php echo esc_attr($ops['name']); ?>">
            </div>
        </div><!-- .control-group -->

        <div class="control-group">
            <label class="control-label" for="input-title">Book Name</label>
            <p><?php _e("Name for your eBook here.", 'bookpress'); ?></p>
            <div class="controls">
                <input type="text" data-id="<?php echo esc_attr($id); ?>" name="input-title" class="span8" value="<?php echo esc_attr($ops['title']); ?>">
            </div>
        </div><!-- .control-group -->

        <div class="control-group">
            <label class="control-label" for="input-cover">Cover Image URL</label>
            <p><?php _e("Cover Image URL for your eBook.<br />This image will display on book stores. (e.g. Kindle Store, iBookstore.)<br />The image should be at least 1000 pixels on the longest side and the ideal height/width ration is 1:1.6.<br />At iBookstore, you will need 1400px on the longest side. You can reupload it when registering the eBook to iBookstore,", 'bookpress'); ?></p>
            <div class="controls">
                <div class="input-append">
                <input type="text" id="input-cover-<?php echo esc_attr($id); ?>" name="input-cover" class="span4" value="<?php echo esc_attr(@$ops['cover']); ?>" placeholder="http://example.com/image.png">
                <input class="btn upload_image_button" data-id="<?php echo esc_attr($id); ?>" type="button" value="Select an Image" />
                </div>
                <div id="cover-preview-<?php echo esc_attr($id); ?>" class="cover-preview"><?php if(@$ops['cover']): ?><img src="<?php echo esc_attr($ops['cover']); ?>" /><?php endif; ?></div>
            </div>
        </div><!-- .control-group -->

        <button type="submit" class="btn btn-primary btn-large pull-right">Save Book</button>
    </form>

    </div>
<?php endforeach; ?>
    </div>
</div>

</div><!-- #manage-books -->


